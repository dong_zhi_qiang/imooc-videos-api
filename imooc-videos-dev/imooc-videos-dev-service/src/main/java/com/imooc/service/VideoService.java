package com.imooc.service;

import java.util.List;

import com.imooc.pojo.Bgm;
import com.imooc.pojo.Comments;
import com.imooc.pojo.Videos;
import com.imooc.utils.PagedResult;

/**
 * @author Chakid
 *
 */

public interface VideoService {
		 
	
	
	/**
	 *     
	 * @param video
	 * @return 保存视频信息到数据库
	 */ 
	
	public String save(Videos video);
	
	/**
	 *    
	 * @param videoId
	 * @param coverPath
	 * @return  修改视频上传封面
	 */
	public void updateVideo(String videoId,String coverPath);
	
	
	/**
	 *   
	 * @param page
	 * @param pageSize
	 * @return 分页查询视频列表
	 */
	
	public PagedResult getAllVideos( Videos video,Integer isSaveRecord,Integer page,Integer pageSize);
	
	/**
	 * 
	 * @return 热搜词
	 */
	public List<String> getHotWords();
	
	/**
	 *   用户喜欢/点赞视频
	 * @param userId
	 * @param videoId
	 * @param videoCreateId
	 */
	public void userLikeVideo(String userId,String videoId,String videoCreateId);
	
	
	/**
	 *   用户不喜欢/取消点赞视频
	 * @param userId
	 * @param videoId
	 * @param videoCreateId
	 */
	public void userUnLikeVideo(String userId,String videoId,String videoCreateId);
	
	/**
	 * @Description: 查询我喜欢的视频列表
	 */
	public PagedResult queryMyLikeVideos(String userId, Integer page, Integer pageSize);
	
	/**
	 * @Description: 查询我关注的人的视频列表
	 */
	public PagedResult queryMyFollowVideos(String userId, Integer page, Integer pageSize);

	
	/**
	 *   保存留言
	 */
	public void saveComment(Comments comment);
	
	
	/**
	 *   留言分页
	 * @param videoId
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public PagedResult getAllComments(String videoId,Integer page,Integer pageSize);
	
}

























